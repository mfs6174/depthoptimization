#ifndef MOTION_COMFORT_H
#define MOTION_COMFORT_H


class MotionComfort
{
public:
	MotionComfort();
	MotionComfort(double vxy, double vz, double d, double lf );
	void init(double vxy, double vz, double d, double lf );
	void truncateParameters();

	double motionComfort( bool include_frequency = 1 );
	double cost();

	double m_speedZ;
	double m_speedXY;
	double m_disparity;
	double m_luminanceFrequency;
	
};

#endif