#include "StereoUtil.h"
#include "SLIC.h"
#include "global_header.h"

void DisparityConverter::init( ViewParameter vp )
{
	m_viewParameter = vp;
	m_zeroAngle = 2*180.0*atan(0.5 * vp.m_eyeInterVal/vp.m_viewDistanceInMeter)/acos(-1.0);
}

DisparityConverter::DisparityConverter()
{
	
}

DisparityConverter::DisparityConverter( ViewParameter vp ):m_viewParameter(vp)
{
	m_zeroAngle = 2*180.0*atan(0.5 * vp.m_eyeInterVal/vp.m_viewDistanceInMeter)/acos(-1.0);
}


double DisparityConverter::cvtPixel2Angle( double pixelValue )
{
	double PixelSize = m_viewParameter.m_pixelSize;
	double EyeHalf = m_viewParameter.m_eyeInterVal * 0.5;
	double ScreenDist = m_viewParameter.m_viewDistanceInMeter;


	double pixelDist = pixelValue * PixelSize;
	double interDist = EyeHalf * ScreenDist / (EyeHalf-pixelDist);
	double interAngle = 2*180*atan(EyeHalf/interDist)/acos(-1.0);
	return m_zeroAngle - interAngle;

}

double DisparityConverter::cvtAngle2Pixel( double angle )
{
	double PixelSize = m_viewParameter.m_pixelSize;
	double EyeHalf = m_viewParameter.m_eyeInterVal * 0.5;
	double ScreenDist = m_viewParameter.m_viewDistanceInMeter;

	float interAngle = m_zeroAngle - angle;
	float interDist = EyeHalf / tan(interAngle/2.0/180.0*acos(-1.0));
	float pixelDist = (interDist-ScreenDist) * tan(interAngle/2.0/180.0*acos(-1.0));
	return pixelDist  / PixelSize;
}

double DisparityConverter::cvtOnDisplayPixel2Angle( Point2f on_display_init, Point2f on_display_target )
{
	double PixelSize = m_viewParameter.m_pixelSize;
	double EyeHalf = m_viewParameter.m_eyeInterVal * 0.5;
	double ScreenDist = m_viewParameter.m_viewDistanceInMeter;
	double DisplayX_Half = m_viewParameter.m_displayWidthInMeter/2;
	double DisplayY_Half = m_viewParameter.m_displayHeightInMeter/2;
	Vec3f _eye_point(DisplayX_Half, DisplayY_Half, ScreenDist);
	Vec3f Init(on_display_init.x*PixelSize, on_display_init.y*PixelSize, 0);
	Vec3f Target(on_display_target.x*PixelSize, on_display_target.y*PixelSize,0);
	Vec3f vector1 = Init - _eye_point;
	Vec3f vector2 = Target - _eye_point;
	double fenzi = vector1.dot(vector2);
	double fenmu = norm(vector1)* norm(vector2);
	double xx = fenzi / fenmu;
	if(fabs(xx - 1)<0.0001)
		xx = 1;
	if(fabs(xx + 1)<0.0001)
		xx = -1;

	float res = fabs(180*acos(xx)/acos(-1.0));
	if(isnan(res))
		cout<<"fun"<<endl;
	return res;
}


void writeMat( Mat &image, string file, string prefix )
{
	FileStorage fout(file, FileStorage::WRITE);
	fout<<prefix<<image;
	fout.release();
}

void readMat( Mat &image, string file, string prefix )
{
	FileStorage fin(file, FileStorage::READ);
	fin[prefix]>>image;
	fin.release();
}

bool writeMatBinary( Mat & image, string file )
{
	ofstream f(file.c_str(), ios::binary|ios::trunc);
	if(!f.is_open())
		return 0;
	int rows = image.rows;
	int cols = image.cols;
	f.write((char *) & rows, sizeof(int));
	f.write((char *) & cols, sizeof(int));
	f.write((char *) image.data, sizeof(float) * rows * cols);
	f.close();
	return 1;
}

bool readMatBinary( Mat & image, string file )
{
	ifstream f(file.c_str(), ios::binary);

	if(!f.is_open())
		return 0;
	int rows, cols;
	f.read( (char*) & rows, sizeof(int));
	f.read( (char*) & cols, sizeof(int));
	image.create(rows, cols, CV_32FC1);
	f.read( (char*) image.data, sizeof(float) * rows * cols);
	return 1;
}

inline unsigned int Vec3b2UBUFF(Vec3b a){ return ((a[2]&0xff)<<16)|((a[1]&0xff)<<8)|(((a[0]&0xff)));}//bgr}

int slic( Mat & image, Mat & seg, int superpixel_size /*= 2500*/, float compactness /*= 20*/ )
{
	int w = image.cols;
	int h = image.rows; 
	unsigned int * ubuff = new unsigned int[w*h];
	FOR_PIXELS(y,x,image){
		ubuff[y*w+x] = Vec3b2UBUFF(image.at<Vec3b>(y,x));
	}
	SLIC slic;
	int * labels;
	int nlabels;

	//int superpixel_size = 2500;
	//float compactness = 20;
	int K = 200;
	//slic.DoSuperpixelSegmentation_ForGivenSuperpixelSize(ubuff, w, h, labels, nlabels, superpixel_size, compactness);
		slic.DoSuperpixelSegmentation_ForGivenNumberOfSuperpixels(ubuff, w, h, labels, nlabels, K, 10);

	//slic.DrawContoursAroundSegments(ubuff, labels, w, h, 0xffffffff);

	seg = Mat::zeros(h,w,CV_32SC1);
	Mat show = Mat::zeros(h,w,CV_8UC3);
	map<int, Vec3b> colormap;
	FOR_PIXELS(y,x,seg){
		seg.at<int>(y,x)= labels[y*w+x];
		/*if(colormap.find(labels[y*w+x])==colormap.end()){
			colormap[labels[y*w+x]] = Vec3b(rand()%255,rand()%255,rand()%255);
		}else{
			show.at<Vec3b>(y,x) = colormap[labels[y*w+x]];
		}
		Vec3b bgr;
		bgr[0] = ubuff[y*w+x]&0xff;
		bgr[1] = (ubuff[y*w+x]>>8)&0xff;
		bgr[2] = (ubuff[y*w+x]>>16)&0xff;
		show.at<Vec3b>(y,x) = bgr;*/
	}
	//SHOW_IMG(show);
	if(labels)
		delete []labels;
	return nlabels;
}


void stereoFromPairs( Mat & left, Mat & right, Mat & out, int offset)
{
	Mat left2 = Mat::zeros(right.size(),CV_8UC3);
	Mat right2 = Mat::zeros(left.size(),CV_8UC3);
	FOR(i,0,left.cols-offset){
		left.col(i+offset).copyTo(left2.col(i));
		right.col(i).copyTo(right2.col(i+offset));
	}
	vector<Mat> left_layers, right_layers, mix_layers;
	split(left2, left_layers);
	split(right2, right_layers);
	mix_layers.push_back(right_layers[0]);
	mix_layers.push_back(right_layers[1]);
	mix_layers.push_back(left_layers[2]);
	merge(mix_layers,out);
}

Mat visualizeBins( vector<float> & vals, int row, int col, int idx )
{
	Mat visu(row, col, CV_8UC3);
	visu.setTo(Scalar(255,255,255));
	int bin_width = col/vals.size();
	float max_val = *max_element(vals.begin(),vals.end()); 
	float min_val = *min_element(vals.begin(), vals.end());
	//cout<<min_val<<" "<<max_val<<endl;
	int unit_height_per_val = row/(max_val + 1e-4f);

	float unit_val_per_height = max_val / row;
	
	if(idx>=0){
		line(visu, Point(bin_width*(idx+0.5), 0), Point(bin_width*(idx+0.5), row), CV_RGB(255,0,0), 1, CV_AA);
	}

	for(int i = 0; i< vals.size();++i){
		rectangle(visu, Point( bin_width* i, 0), Point(bin_width*(i+1), unit_height_per_val * vals[i]), CV_RGB(0,200,0), -1, CV_AA);
		rectangle(visu, Point( bin_width* i, 0), Point(bin_width*(i+1), unit_height_per_val * vals[i]), CV_RGB(0,255,0), 1, CV_AA);
	}
	int mmd = vals.size()/ 80;
	flip(visu, visu, 0);
	for(int i = 0; i < vals.size(); ++ i){
		if(i% mmd == 0){
			char str_i[11];
			sprintf(str_i,"%d",(i));

			putText(visu, str_i, cv::Point(bin_width*(i+0.5), 20),CV_FONT_HERSHEY_COMPLEX,0.5,Scalar(0,0,0), 1, CV_AA);
		}
	}

	char str_i[11];
	float val = max_val;
	sprintf(str_i,"%f",(val));
	putText(visu, str_i, cv::Point(10, 15),CV_FONT_HERSHEY_COMPLEX,0.5,Scalar(0,0,255), 1, CV_AA);
	for(int i = 1; i < row; ++ i){ 
		if(i% 40 == 0){
			char str_i[11];
			float val = (row - i) * unit_val_per_height;
			sprintf(str_i,"%f",(val));

			putText(visu, str_i, cv::Point(10, i),CV_FONT_HERSHEY_COMPLEX,0.5,Scalar(0,0,0), 1, CV_AA);
		}
	}
	return visu;
}

