#include "new_evaluation.h"

#include <boost/lexical_cast.hpp>
#include <boost/filesystem.hpp>
double evaluation::stereo_motion_term( Mat & disparity_map, Mat & motion2DX, Mat & motion2DY, vector<int> & ori_disp, vector<int> & target_disp )
{

}

double evaluation::comfort_zone_term( Mat & disparity_map, vector<int> & ori_disp, vector<int> & target_disp )
{
	//step 1. Get new disparity map
	Mat tar_disp_map = evaluation_util::target_disparity(disparity_map, ori_disp, target_disp);


	//step 2. Get importance map
	Mat focus_weight(tar_disp_map.size(), CV_32FC1);

	int col = focus_weight.cols;
	int row = focus_weight.rows;

	FOR(y, 0, focus_weight.rows){
		float * ptrFocus = focus_weight.ptr<float>(y);
		FOR(x, 0, focus_weight.cols){
			ptrFocus[x] = exp(-0.0001*((x - col/2)*(x - col/2) + (y - row/2)*(y - row/2)));
		}
	}

	normalize(focus_weight, focus_weight, 0, 1, NORM_MINMAX);
	
	float fenmu = cv::sum(focus_weight)[0];

	//step 3. Analyse disparity value

	ViewParameter vp("config.txt");
	DisparityConverter dc(vp);

	double sum = 0;
	FOR(y, 0, row){
		float * ptrDisp = tar_disp_map.ptr<float>(y);
		float * ptrWeight = focus_weight.ptr<float>(y);
		FOR(x, 0, col){
			ExcessiveDepthCost edc(dc.cvtPixel2Angle(ptrDisp[x]));
			sum += edc.cost() * ptrWeight[x];
		}
	}
	sum/=fenmu;


	return sum;


}

double evaluation::neighbour_smooth_term( Mat & disparity_map1, vector<int> & ori_disp1, vector<int> & target_disp1, Mat & disparity_map2, vector<int> & ori_disp2, vector<int> & target_disp2 )
{
	Mat target1 = evaluation_util::target_disparity(disparity_map1, ori_disp1, target_disp1);

	Mat target2 = evaluation_util::target_disparity(disparity_map2, ori_disp2, target_disp2);


	int min_bin = -100, max_bin = 100;

	int bin_num = max_bin - min_bin +1;
	int histSize[] = {bin_num};
	float disp_range[] = {min_bin, max_bin};
	const float * ranges[] = {disp_range};
	MatND hist1, hist2;
	int channels[] = {0};







	calcHist(&target1, 1, channels, Mat(), hist1, 1, histSize, ranges, true, false);

	calcHist(&target2, 1, channels, Mat(), hist2, 1, histSize, ranges, true, false);
	

	//cout<<hist1.size()<<" "<<hist2.size()<<endl;



	//SHOW histogram

/*
	Mat show_hist(500, bin_num * 10, CV_8UC3);
	show_hist.setTo(Scalar(255,255,255));

	float sum = 0;
	FOR(i, 0, bin_num){
		sum+=hist1.at<float>(i,0);
	}
	//float sum = 0;
	if(sum>0)
	{
		FOR(i, 0, bin_num){
			hist1.at<float>(i,0) = hist1.at<float>(i,0)/sum;
		}
	}
//	cout<<sum<<endl;
	FOR(i,0,bin_num){
		int height = 500 * hist1.at<float>(i,0)/sum;
		if(height<=0)
			continue;
		if(height>=500)
			height = 500;
		rectangle(show_hist, Point(i*10, 0), Point((i+1)*10, height),CV_RGB(150,0,0), -1);
		rectangle(show_hist, Point(i*10, 0), Point((i+1)*10, height), CV_RGB(250,0,0), 2);
	}
//	SHOW_IMG(show_hist);

	{
		Mat show_hist2(500, bin_num * 10, CV_8UC3);
		show_hist2.setTo(Scalar(255,255,255));

		float sum = 0;
		FOR(i, 0, bin_num){
			sum+=hist2.at<float>(i,0);
		}
		if(sum>0)
		{
			FOR(i, 0, bin_num){
				hist2.at<float>(i,0) = hist2.at<float>(i,0)/sum;
			}
		}
	//	cout<< hist2.col(0);
		FOR(i,0,bin_num){
			int height = 500 * hist2.at<float>(0,i)/sum;
			if(height<=0)
				continue;
			if(height>=500)
				height = 500;
			rectangle(show_hist2, Point(i*10, 0), Point((i+1)*10, height),CV_RGB(150,0,0), -1);
			rectangle(show_hist2, Point(i*10, 0), Point((i+1)*10, height), CV_RGB(250,0,0),2);
		}
	//	SHOW_IMG(show_hist2);

	}*/

/*
	Mat h1 = hist1.row(0);
	h1 = h1.t();
	Mat h2 = hist2.row(0);
	h2 = h2.t();*/

	Mat h1(hist1.rows, 2, CV_32FC1), h2(hist2.rows, 2, CV_32FC1);
	//cout<<h1.size()<<" "<<h2.size()<<endl;
	FOR(i, 0, hist1.rows){
		h1.at<float>(i, 0) = hist1.at<float>(i,0);
		h1.at<float>(i, 1) = i;
	}
	FOR(i, 0, hist2.rows){
		h2.at<float>(i, 0) = hist2.at<float>(i,0);
		h2.at<float>(i, 1) = i;
	}
	return EMD(h1, h2, CV_DIST_L2);
	//return compareHist(hist1, hist2, CV_COMP_BHATTACHARYYA);
}

double evaluation::stereo_window_violation_term( Mat & disparity_map, Mat & reverse_disparity_map, vector<int> & ori_disp, vector<int> & target_disp )
{
	//Step 1. Update Disparity & Reverse Disparity Maps

	map< int, int> disp_map;
	map< int, int> rev_disp_map;
	FOR(i, 0, ori_disp.size()){
		disp_map[ori_disp[i]] = target_disp[i];
		rev_disp_map[-ori_disp[i]] = - target_disp[i];
	}
	
	Mat new_disp(disparity_map.size(),CV_32FC1);
	Mat new_rev_disp(reverse_disparity_map.size(),CV_32FC1);

	FOR(y, 0, disparity_map.rows){
		float * ptrOri = disparity_map.ptr<float>(y);
		float * ptrRevOri = reverse_disparity_map.ptr<float>(y);
		float * ptrNewDisp = new_disp.ptr<float>(y);
		float * ptrNewRevDisp = new_rev_disp.ptr<float>(y);

		FOR(x, 0, disparity_map.cols){
			ptrNewDisp[x] = disp_map[(int)ptrOri[x]];
			ptrNewRevDisp[x] = rev_disp_map[(int)ptrRevOri[x]];
		}
	}

	//Step 2. Calculate SWV Areas

	int swv_pixel_cnt = 0;

	Mat illu_leftbord = Mat::zeros(reverse_disparity_map.size(),CV_8UC1);
	FOR(y, 0, illu_leftbord.rows){
		float * ptrRevDisp = new_rev_disp.ptr<float>(y);
		uchar * ptrIllu = illu_leftbord.ptr<uchar>(y);
		int x = 0;
		while(x<illu_leftbord.cols){
			if(ptrRevDisp[x]<=0)
				break;
			ptrIllu[x] = 255;
			swv_pixel_cnt++;
			x++;
		}
	}

/*
	SHOW_IMG(disparity_map);
	SHOW_IMG(reverse_disparity_map);*/
	Mat illu_rightbord = Mat::zeros(disparity_map.size(),CV_8UC1);
	FOR(y, 0, illu_rightbord.rows){
		float * ptrDisp = new_disp.ptr<float>(y);
		uchar * ptrIllu = illu_rightbord.ptr<uchar>(y);
		int x = illu_rightbord.cols-1;
		while(x>=0){
			if(ptrDisp[x]>=0)
				break;
			ptrIllu[x] = 255;
			swv_pixel_cnt++;
			x--;
		}
	}
	Mat show;
	bitwise_or(illu_leftbord, illu_rightbord, show);
	SHOW_IMG(show);
	return swv_pixel_cnt/(float)(disparity_map.cols * disparity_map.rows);
}

double evaluation::stereo_window_violation_term2( Mat & disparity_map, Mat & reverse_disparity_map, vector<int> & ori_disp, vector<int> & target_disp )
{
	//Step 1. Update Disparity & Reverse Disparity Maps

	map< int, int> disp_mapper;
	map< int, int> rev_disp_mapper;
	FOR(i, 0, ori_disp.size()){
		disp_mapper[ori_disp[i]] = target_disp[i];
		rev_disp_mapper[-ori_disp[i]] = - target_disp[i];
	}
	
	Mat new_disp(disparity_map.size(),CV_32FC1);
	Mat new_rev_disp(reverse_disparity_map.size(),CV_32FC1);

	FOR(y, 0, disparity_map.rows){
		float * ptrOri = disparity_map.ptr<float>(y);
		float * ptrRevOri = reverse_disparity_map.ptr<float>(y);
		float * ptrNewDisp = new_disp.ptr<float>(y);
		float * ptrNewRevDisp = new_rev_disp.ptr<float>(y);

		FOR(x, 0, disparity_map.cols){
			ptrNewDisp[x] = disp_mapper[(int)ptrOri[x]];
			ptrNewRevDisp[x] = rev_disp_mapper[(int)ptrRevOri[x]];
		}
	}

	//Step 2. Calculate SWV Areas

	int swv_pixel_cnt = 0;

	Mat illu_leftbord = Mat::zeros(reverse_disparity_map.size(),CV_8UC1);
	int max_x = 0;
	FOR(y, 0, illu_leftbord.rows){
		float val = new_rev_disp.at<float>(y,0);
		if(val>max_x)
			max_x = val;
	}

	FOR(y, 0, illu_leftbord.rows){
		float * ptrDisp = new_disp.ptr<float>(y);
		uchar * ptrIllu = illu_leftbord.ptr<uchar>(y);
		FOR(x, 0, max_x){
			if(ptrDisp[x]<0){
				swv_pixel_cnt++;
				ptrIllu[x] = 255;
			}
		}
	}

	Mat illu_rightbord = Mat::zeros(disparity_map.size(),CV_8UC1);
	int min_x = 0;
	FOR(y, 0, illu_rightbord.rows){
		float val = new_disp.at<float>(y,illu_rightbord.cols-1);
		if(val<min_x)
			min_x = val;
	}

	FOR(y, 0, illu_rightbord.rows){
		float * ptrDisp = new_rev_disp.ptr<float>(y);
		uchar * ptrIllu = illu_rightbord.ptr<uchar>(y);
		FOR(x, illu_rightbord.cols + min_x, illu_rightbord.cols){
			if(ptrDisp[x]>0){
				swv_pixel_cnt++;
				ptrIllu[x] = 255;
			}
		}
	}



	//Mat show;
	//bitwise_or(illu_leftbord, illu_rightbord, show);
	//SHOW_IMG(show);
	return swv_pixel_cnt/(float)(disparity_map.cols * disparity_map.rows);
}

double evaluation::do_not_change_term( Mat & disparity1, vector<int> & ori_disp1, vector<int> & tar_disp1)
{
/*
	Mat target1 = evaluation_util::target_disparity(disparity1, ori_disp1, tar_disp1);

	int min_bin = -100, max_bin = 100;

	int bin_num = max_bin - min_bin +1;
	int histSize[] = {bin_num};
	float disp_range[] = {min_bin, max_bin};
	const float * ranges[] = {disp_range};
	MatND hist1, hist2;
	int channels[] = {0};







	calcHist(&target1, 1, channels, Mat(), hist1, 1, histSize, ranges, true, false);

	calcHist(&disparity1, 1, channels, Mat(), hist2, 1, histSize, ranges, true, false);






	//SHOW histogram

	{
		Mat show_hist(500, bin_num * 10, CV_8UC3);
		show_hist.setTo(Scalar(255,255,255));

		float sum = 0;
		FOR(i, 0, bin_num){
			sum+=hist1.at<float>(0,i);
		}
		cout<<sum<<endl;
		FOR(i,0,bin_num){
			int height = 500 * hist1.at<float>(0,i)/sum;
			if(height<=0)
				continue;
			if(height>=500)
				height = 500;
			rectangle(show_hist, Point(i*10, 0), Point((i+1)*10, height),CV_RGB(150,0,0), -1);
			rectangle(show_hist, Point(i*10, 0), Point((i+1)*10, height), CV_RGB(250,0,0), 2);
		}
		SHOW_IMG(show_hist);
	}

	{
		Mat show_hist2(500, bin_num * 10, CV_8UC3);
		show_hist2.setTo(Scalar(255,255,255));

		float sum = 0;
		FOR(i, 0, bin_num){
			sum+=hist2.at<float>(0,i);
		}
		FOR(i,0,bin_num){
			int height = 500 * hist2.at<float>(0,i)/sum;
			if(height<=0)
				continue;
			if(height>=500)
				height = 500;
			rectangle(show_hist2, Point(i*10, 0), Point((i+1)*10, height),CV_RGB(150,0,0), -1);
			rectangle(show_hist2, Point(i*10, 0), Point((i+1)*10, height), CV_RGB(250,0,0),2);
		}
		SHOW_IMG(show_hist2);

	}




*/

	map<int, int> hist;
	FOR(y, 0, disparity1.rows){
		float * ptrDisp = disparity1.ptr<float>(y);
		FOR(x, 0, disparity1.cols){
			int val = (int)ptrDisp[x];
			if(hist.find(val) == hist.end())
				hist[val] = 1;
			else hist[val]++;
		}
	}

	map<int, int>::iterator it = hist.begin();
	int pix = 0;
	for(;it!= hist.end(); ++it){
		//cout<<it->first<<" "<<it->second<<endl;
		pix+=it->second;
	}

	double sum = 0;
	double area = disparity1.cols * disparity1.rows;
	//cout<<pix<<(int)area<<endl;
	//cout<<ori_disp1.size()<<endl;
	//cout<<tar_disp1.size()<<endl;
	for(int i = 0; i< ori_disp1.size(); ++ i){
		int idx = ori_disp1[i];
		if(hist.find(idx) == hist.end())
			continue;
		else{
			int tot = hist[idx];
			double v =  tot * abs(idx - tar_disp1[i])/area; 
			sum+=v;
			/*if(idx == - 49){
				cout<<tot<<endl;
				cout<<idx<<endl;
				cout<<tar_disp1[i]<<endl;
				cout<<v<<endl;
			}*/
		}
	}
//	cout<<sum<<endl;
	/*for(;it!=hist.end(); ++it){
		int idx = it->first;
		int tot = it->second;
		//if(idx>400 || idx< -400){
		//	cout<<"fuck: "<<idx<<endl;
		//}
		if(ori_disp1[idx]!= tar_disp1[idx]){
			cout<<idx<<endl;
			cout<<ori_disp1[idx]<<endl;
			cout<<tar_disp1[idx]<<endl;
			cout<<"fff"<<endl;
		}
		double v =  tot * abs(ori_disp1[idx] - tar_disp1[idx])/area; 
		sum += v;
	}
	if(fabs(sum)>0){
		cout<<"!!!!Warning!!!: "<<sum<<endl;
	}*/
	return sum;
}



/*
double evaluation::motion_comfort_term_mask( Mat & mask, Mat & pX, Mat & pY, Mat & pD1, vector<int> & map_ori1, vector<int> & map_tar1, Mat & pD2, vector<int> & map_ori2, vector<int> & map_tar2 )
{
	Mat D1 = evaluation_util::target_disparity(pD1, map_ori1, map_tar1);
	Mat D2 = evaluation_util::target_disparity(pD2, map_ori2, map_tar2);

	evaluation_util::MotionHelper mh;
	Mat MXY, MZ;
	mh.getAngularMotion(D1, D2, pX, pY, MXY, MZ);
	resize(mask, mask, pX.size());
	erode(mask, mask, Mat(), Point(-1,-1),2);
	//mask.setTo(255);
	ViewParameter vp;
	DisparityConverter dc(vp);

	Mat mcVal = Mat::zeros(pX.size(), CV_32FC1);
	Mat show = Mat::zeros(pX.size(), CV_8UC1);
	float acc_mc = 0;
	FOR(y, 0, pX.rows){
		float * pDs1 = D1.ptr<float>(y);
		float * pXY = MXY.ptr<float>(y);
		float * pZ = MZ.ptr<float>(y);
		float * pM = mask.ptr<float>(y);
		float * pVal = mcVal.ptr<float>(y);
		uchar * pS = show.ptr<uchar>(y);
		FOR(x, 0, pX.cols){
			if(pM[x]>0){
				float d = dc.cvtPixel2Angle(pDs1[x]);
				MotionComfort mc(pXY[x], pZ[x], -d, 8);
				pVal[x] = 5 - mc.cost();
				acc_mc += pVal[x];
				pS[x] = 40 * (int)pVal[x];
			}
		}
	}
	 
	Mat pV;
	normalize(mcVal, pV, 0, 1, NORM_MINMAX);
	SHOW_IMG(pV);
	return acc_mc/countNonZero(mask);
}

double evaluation::motion_comfort_term_saliency(Mat & saliency, Mat & pX, Mat & pY, Mat & pD1, vector<int> & map_ori1, vector<int> & map_tar1, Mat & pD2, vector<int> & map_ori2, vector<int> & map_tar2 )
{
	Mat D1 = evaluation_util::target_disparity(pD1, map_ori1, map_tar1);
	Mat D2 = evaluation_util::target_disparity(pD2, map_ori2, map_tar2);

	evaluation_util::MotionHelper mh;
	Mat MXY, MZ;
	mh.getAngularMotion(D1, D2, pX, pY, MXY, MZ);
	//resize(mask, mask, pX.size());
	//erode(mask, mask, Mat(), Point(-1,-1),2);
	//mask.setTo(255);
	ViewParameter vp;
	DisparityConverter dc(vp);

	Mat mcVal = Mat::zeros(pX.size(), CV_32FC1);
	//Mat show = Mat::zeros(pX.size(), CV_8UC1);
	float acc_mc = 0;
	FOR(y, 0, pX.rows){
		float * pDs1 = D1.ptr<float>(y);
		float * pXY = MXY.ptr<float>(y);
		float * pZ = MZ.ptr<float>(y);
		float * pSal = saliency.ptr<float>(y);
		float * pVal = mcVal.ptr<float>(y);
		//uchar * pS = show.ptr<uchar>(y);
		FOR(x, 0, pX.cols){
			//if(pM[x]>0){
				float d = dc.cvtPixel2Angle(pDs1[x]);
				MotionComfort mc(pXY[x], pZ[x], -d, 8);
				pVal[x] = pSal[x]*(5 - mc.cost());
				acc_mc += pVal[x];
			//	pS[x] = 40 * (int)pVal[x];
			//}
		}
	}

	Mat pV;
	normalize(mcVal, pV, 0, 1, NORM_MINMAX);
	SHOW_IMG(saliency);
	SHOW_IMG(pV);
	return acc_mc/(mcVal.rows * mcVal.cols);
}
*/

int ccc = 0;

double evaluation::motion_comfort_term(Mat & seg, Mat & pX, Mat & pY, Mat & pD1, vector<int> & map_ori1, vector<int> & map_tar1, Mat & pD2, vector<int> & map_ori2, vector<int> & map_tar2 )
{
	Mat D1 = evaluation_util::target_disparity(pD1, map_ori1, map_tar1);
	Mat D2 = evaluation_util::target_disparity(pD2, map_ori2, map_tar2);

	evaluation_util::MotionHelper mh;
	Mat MXY, MZ;
	mh.getAngularMotion(D1, D2, pX, pY, MXY, MZ);
	//resize(mask, mask, pX.size());
	//erode(mask, mask, Mat(), Point(-1,-1),2);
	//mask.setTo(255);
	ViewParameter vp;
	DisparityConverter dc(vp);

	Mat mcVal = Mat::zeros(pX.size(), CV_32FC1);

	map<int,int> segmap;
	map<int,float> segval;
	float acc_mc = 0;
	int max_seg = -1;
	FOR(y, 0, pX.rows){
		float * pDs1 = D1.ptr<float>(y);
		float * pXY = MXY.ptr<float>(y);
		float * pZ = MZ.ptr<float>(y);
		float * pVal = mcVal.ptr<float>(y);
		int * pSeg = seg.ptr<int>(y);

		FOR(x, 0, pX.cols){

			float d = dc.cvtPixel2Angle(pDs1[x]);
			MotionComfort mc(pXY[x], pZ[x], -d, 8);
			pVal[x] = (5 - mc.cost());
			acc_mc += pVal[x];
			max_seg = max(max_seg, pSeg[x]);
		}
	}
	vector<int> scnt(max_seg+1, 0);
	vector<float> sval(max_seg+1, 0);

	FOR(y, 0, pX.rows){
		float * pVal = mcVal.ptr<float>(y);
		int * pSeg = seg.ptr<int>(y);

		FOR(x, 0, pX.cols){
			scnt[pSeg[x]]++;
			sval[pSeg[x]] += pVal[x];
		}
	}

	//seg.convertTo(seg, CV_8UC1, 1);
	//SHOW_IMG(seg);

	{
		struct mystruct{
			mystruct(int i = -1, float v = -1){
				idx = i;
				val = v;
			}
			int idx;
			float val;
			bool operator<(const mystruct & rhs)const{
				return val> rhs.val;
			}
		};
		vector<mystruct> mysortvector;
		FOR(i, 0 , scnt.size()){
			mysortvector.push_back(mystruct(i, sval[i]/scnt[i]));
		}
		sort(mysortvector.begin(), mysortvector.end());

		vector<int> topc;
		double return_val = 0;
		FOR(i, 0, 20){
			topc.push_back(mysortvector[i].idx);
			return_val += mysortvector[i].val/20.f;
		}
		// Mat myshow(seg.size(),CV_8UC1);
		// FOR_PIXELS(y,x,seg){
		// 	int val = seg.at<int>(y,x);
		// 	FOR(i,0, topc.size()){
		// 		if(val == topc[i]){
		// 			myshow.at<uchar>(y,x) = 255 * (topc.size() - i)/ topc.size();
		// 		}
		// 	}
		// }
		// imwrite(boost::lexical_cast<string>(ccc++)+".png", myshow);
		return return_val;
	
	}
	
	float max_val = -1;
	FOR(i, 0, scnt.size()){
		max_val = max(max_val, sval[i]/scnt[i]);
	}
	return max_val;
	//Mat pV;
	//normalize(mcVal, pV, 0, 1, NORM_MINMAX);

	//SHOW_IMG(pV);
	return acc_mc/(mcVal.rows * mcVal.cols);
}

/*
void evaluation::optimize( _Param mp, \
	int start_frame, vector<int> start_xcoord, vector<int> start_ycoord, \
	int end_frame, vector<int> end_xcoord, vector<int> end_ycoord, \
	vector<int> to_opti_frames, vector< vector<int> > to_opti_xcoords,  vector< vector<int> >  to_opti_ycoords )
{

	double cost = get_cost(mp, \
		start_frame, start_xcoord,  start_ycoord, \
		 end_frame,  end_xcoord,  end_ycoord, \
		 to_opti_frames,  to_opti_xcoords,  to_opti_ycoords);

}*/

/*
double evaluation::get_cost( _Param mp, \
	int start_frame, vector<int> start_xcoord, vector<int> start_ycoord, \
	int end_frame, vector<int> end_xcoord, vector<int> end_ycoord, \
	vector<int> to_opti_frames, vector< vector<int> > to_opti_xcoords,  vector< vector<int> >  to_opti_ycoords )
{
	
	double w_cfz = 1.0, w_swv = 50, w_smooth = 0.2, w_motion = 0.5;

	string disp_folder = mp.m_disparity_folder;
	string motion_folder = mp.m_motion_folder;
	string seg_folder = mp.m_seg_folder;

	Mat start_disp, r_start_disp;
	readMatBinary(start_disp, disp_folder + "/" + boost::lexical_cast<string>(start_frame));
	readMatBinary(r_start_disp, disp_folder + "/r" + boost::lexical_cast<string>(start_frame));
	Mat end_disp, r_end_disp;
	readMatBinary(end_disp, disp_folder + "/" + boost::lexical_cast<string>(end_frame));
	readMatBinary(r_end_disp, disp_folder + "/r" + boost::lexical_cast<string>(end_frame));

	double val = 0;
	
	val += w_cfz * comfort_zone_term(start_disp, start_xcoord, start_ycoord);
	val += w_swv * stereo_window_violation_term2(start_disp, r_start_disp, start_xcoord, start_ycoord );
	Mat disp, disp_next, r_disp;	
	Mat seg;
	Mat mx, my;


	if(start_frame!=end_frame){
		readMatBinary(seg, seg_folder + "/" + boost::lexical_cast<string>(start_frame));
				seg.convertTo(seg, CV_32SC1);
		readMatBinary(mx, motion_folder + "/" + boost::lexical_cast<string>(start_frame));
		readMatBinary(my, motion_folder + "/" + boost::lexical_cast<string>(start_frame));
		readMatBinary(disp_next, seg_folder + "/" + boost::lexical_cast<string>(start_frame +1));
		val += w_smooth * neighbour_smooth_term(disp, start_xcoord, start_ycoord, disp_next, start_xcoord, start_ycoord);
		val += w_motion * motion_comfort_term(seg, mx, my, start_disp, start_xcoord, start_ycoord, disp_next, start_xcoord, start_ycoord);
	}

	FOR(i, 0, to_opti_frames.size()){
		readMatBinary(disp, disp_folder + "/" + boost::lexical_cast<string>(to_opti_frames[i]));
		readMatBinary(r_disp, disp_folder + "/r" + boost::lexical_cast<string>(to_opti_frames[i]));
		readMatBinary(disp_next, disp_folder + "/" + boost::lexical_cast<string>(to_opti_frames[i] +1));

		//unary_term
		val += w_cfz * comfort_zone_term(disp, to_opti_xcoords[i], to_opti_ycoords[i]);
		val += w_swv * stereo_window_violation_term2(disp, r_disp, to_opti_xcoords[i], to_opti_ycoords[i]);


		readMatBinary(mx, motion_folder + "/x_" + boost::lexical_cast<string>(to_opti_frames[i]));
		readMatBinary(my, motion_folder + "/y_" + boost::lexical_cast<string>(to_opti_frames[i]));
		readMatBinary(seg, seg_folder + "/" + boost::lexical_cast<string>(to_opti_frames[i]));	
				seg.convertTo(seg, CV_32SC1);
		val += w_smooth * neighbour_smooth_term(disp, to_opti_xcoords[i], to_opti_ycoords[i], disp_next, to_opti_xcoords[i], to_opti_ycoords[i]);
		val += w_motion * motion_comfort_term(seg, mx, my, disp, to_opti_xcoords[i], to_opti_ycoords[i], disp_next, to_opti_xcoords[i], to_opti_ycoords[i]);	
	}
	
	val += w_cfz * comfort_zone_term(end_disp, end_xcoord, end_ycoord);
	val += w_swv * stereo_window_violation_term2(end_disp, r_end_disp, end_xcoord, end_ycoord );

	return val;
}*/

double evaluation::get_cost( _Param mp, int idx, vector< vector<int> > & x_coords, vector< vector<int> > & y_coords, vector<bool> fixed)
{

	string disp_folder = mp.m_disparity_folder;
	string motion_folder = mp.m_motion_folder;
	string seg_folder = mp.m_seg_folder;

	double w_cfz = mp.w_cfz, w_swv = mp.w_swv, w_smooth = mp.w_smooth, w_motion = mp.w_motion, w_nochange = mp.w_nochange;
	double val = 0;

	Mat disp, r_disp;
	readMatBinary(disp, disp_folder + "/" + boost::lexical_cast<string>(idx)+".txt");
	readMatBinary(r_disp, disp_folder + "/r" + boost::lexical_cast<string>(idx)+".txt");

/*
	Mat sd;
	normalize(disp, sd, 0, 1, NORM_MINMAX);
	SHOW_IMG(sd);*/
	//unary term
	val += w_cfz * comfort_zone_term(disp, x_coords[idx], y_coords[idx]);
	//cout<<val<<endl;
	//val += w_swv * stereo_window_violation_term2(disp, r_disp, x_coords[idx], y_coords[idx]);
	//double s1 = val;
	//cout<<val<<endl;
	val += w_nochange * do_not_change_term(disp, x_coords[idx], y_coords[idx]);
	//double s2 = val;
/*
	if(s2!=s1){
		cout<<"fucn"<<endl;
	}*/
	//cout<<val<<endl;
	//binary term if needed
	

	/*if(idx > 0){
		Mat seg, mx, my, disp_prev;

		readMatBinary(mx, motion_folder + "/x_" + boost::lexical_cast<string>(idx-1)+".txt");
		readMatBinary(my, motion_folder + "/y_" + boost::lexical_cast<string>(idx-1)+".txt");
		readMatBinary(seg, seg_folder + "/" + boost::lexical_cast<string>(idx-1)+".txt");	
		readMatBinary(disp_prev, disp_folder + "/" + boost::lexical_cast<string>(idx-1)+".txt");	
		seg.convertTo(seg, CV_32SC1);
		if(true == fixed[idx-1]){
			val += w_smooth * neighbour_smooth_term(disp_prev, x_coords[idx-1], y_coords[idx-1], disp, x_coords[idx], y_coords[idx]);
			val += w_motion * motion_comfort_term(seg, mx, my, disp_prev, x_coords[idx-1], y_coords[idx-1], disp, x_coords[idx], y_coords[idx]);
		}else{
			val += w_smooth * neighbour_smooth_term(disp_prev, x_coords[idx], y_coords[idx], disp, x_coords[idx], y_coords[idx]);
			val += w_motion * motion_comfort_term(seg, mx, my, disp_prev, x_coords[idx], y_coords[idx], disp, x_coords[idx], y_coords[idx]);
		}
	}*/

	if(idx<fixed.size()-1){
		Mat seg, mx, my, disp_next;

		readMatBinary(mx, motion_folder + "/x_" + boost::lexical_cast<string>(idx)+".txt");
		readMatBinary(my, motion_folder + "/y_" + boost::lexical_cast<string>(idx)+".txt");
		readMatBinary(seg, seg_folder + "/" + boost::lexical_cast<string>(idx)+".txt");	
		readMatBinary(disp_next, disp_folder + "/" + boost::lexical_cast<string>(idx+1)+".txt");	
		seg.convertTo(seg, CV_32SC1);
		if(true == fixed[idx+1]){
			val += w_smooth * neighbour_smooth_term(disp, x_coords[idx], y_coords[idx], disp_next, x_coords[idx+1], y_coords[idx+1]);
			val += w_motion * motion_comfort_term(seg, mx, my, disp, x_coords[idx], y_coords[idx], disp_next, x_coords[idx+1], y_coords[idx+1]);
		}else{
			val += w_smooth * neighbour_smooth_term(disp, x_coords[idx], y_coords[idx], disp_next, x_coords[idx], y_coords[idx]);
			val += w_motion * motion_comfort_term(seg, mx, my, disp, x_coords[idx], y_coords[idx], disp_next, x_coords[idx], y_coords[idx]);
		}
	}

	return val;
}

double evaluation::get_cost( _Param mp, int idx, vector< vector<int> > & x_coords, vector< vector<int> > & y_coords)
{
	string disp_folder = mp.m_disparity_folder;
	string motion_folder = mp.m_motion_folder;
	string seg_folder = mp.m_seg_folder;

	double w_cfz = mp.w_cfz, w_swv = mp.w_swv, w_smooth = mp.w_smooth, w_motion = mp.w_motion, w_nochange = mp.w_nochange;
	double val = 0;

	Mat disp, r_disp;
	readMatBinary(disp, disp_folder + "/" + boost::lexical_cast<string>(idx)+".txt");
	readMatBinary(r_disp, disp_folder + "/r" + boost::lexical_cast<string>(idx)+".txt");

	vector<bool> fixed(x_coords.size(), 1);

/*
	Mat sd;
	normalize(disp, sd, 0, 1, NORM_MINMAX);
	SHOW_IMG(sd);*/
	//unary term
	val += w_cfz * comfort_zone_term(disp, x_coords[idx], y_coords[idx]);
	//cout<<val<<endl;
	//val += w_swv * stereo_window_violation_term2(disp, r_disp, x_coords[idx], y_coords[idx]);
	//double s1 = val;
	//cout<<val<<endl;
	val += w_nochange * do_not_change_term(disp, x_coords[idx], y_coords[idx]);
	//double s2 = val;
/*
	if(s2!=s1){
		cout<<"fucn"<<endl;
	}*/
	//cout<<val<<endl;
	//binary term if needed
	

	if(idx> 0 && (mp.opti_direction == 2||mp.opti_direction == 0)){
		Mat seg, mx, my, disp_prev;

		readMatBinary(mx, motion_folder + "/x_" + boost::lexical_cast<string>(idx-1)+".txt");
		readMatBinary(my, motion_folder + "/y_" + boost::lexical_cast<string>(idx-1)+".txt");
		readMatBinary(seg, seg_folder + "/" + boost::lexical_cast<string>(idx-1)+".txt");	
		readMatBinary(disp_prev, disp_folder + "/" + boost::lexical_cast<string>(idx-1)+".txt");	
		seg.convertTo(seg, CV_32SC1);
		if(true == fixed[idx-1]){
			val += w_smooth * neighbour_smooth_term(disp_prev, x_coords[idx-1], y_coords[idx-1], disp, x_coords[idx], y_coords[idx]);
			val += w_motion * motion_comfort_term(seg, mx, my, disp_prev, x_coords[idx-1], y_coords[idx-1], disp, x_coords[idx], y_coords[idx]);
		}else{
			val += w_smooth * neighbour_smooth_term(disp_prev, x_coords[idx], y_coords[idx], disp, x_coords[idx], y_coords[idx]);
			val += w_motion * motion_comfort_term(seg, mx, my, disp_prev, x_coords[idx], y_coords[idx], disp, x_coords[idx], y_coords[idx]);
		}
	}
	if((idx<fixed.size()-1) && (mp.opti_direction ==1 || mp.opti_direction == 2)){
		Mat seg, mx, my, disp_next;

		readMatBinary(mx, motion_folder + "/x_" + boost::lexical_cast<string>(idx)+".txt");
		readMatBinary(my, motion_folder + "/y_" + boost::lexical_cast<string>(idx)+".txt");
		readMatBinary(seg, seg_folder + "/" + boost::lexical_cast<string>(idx)+".txt");	
		readMatBinary(disp_next, disp_folder + "/" + boost::lexical_cast<string>(idx+1)+".txt");	
		seg.convertTo(seg, CV_32SC1);
		if(true == fixed[idx+1]){
			val += w_smooth * neighbour_smooth_term(disp, x_coords[idx], y_coords[idx], disp_next, x_coords[idx+1], y_coords[idx+1]);
			val += w_motion * motion_comfort_term(seg, mx, my, disp, x_coords[idx], y_coords[idx], disp_next, x_coords[idx+1], y_coords[idx+1]);
		}else{
			val += w_smooth * neighbour_smooth_term(disp, x_coords[idx], y_coords[idx], disp_next, x_coords[idx], y_coords[idx]);
			val += w_motion * motion_comfort_term(seg, mx, my, disp, x_coords[idx], y_coords[idx], disp_next, x_coords[idx], y_coords[idx]);
		}
	}

	return val;
}

bool evaluation::get_depth_range( _Param mp, int idx, float & mind, float & maxd )
{
  string stem=boost::lexical_cast<string>(idx);
	string file = mp.m_disparity_folder + "/" + stem + ".txt";
	if(!boost::filesystem::exists(file))
		return 0;

	Mat disp;
	readMatBinary(disp, file);
	float minval = 10000, maxval = -10000;
	FOR_PIXELS(y, x, disp){
		if(disp.at<float>(y,x)<minval)
			minval = disp.at<float>(y,x);
		if(disp.at<float>(y,x)>maxval)
			maxval = disp.at<float>(y,x);

	}
	int len = 100;
	if(maxval - minval + 1 <= 100){
		mind = (int)minval-1;
		maxd = (int)maxval+1;
		return 1;
	}

	vector<int> vals;
	vals.resize(maxval - minval +1, 0);

	FOR_PIXELS(y,x,disp){
		vals[disp.at<float>(y,x) - minval] ++;
	}
	vector<int> sum;
	int s = 0;
	FOR(i, 0, vals.size()){
		s += vals[i];
		sum.push_back(s);
	}
	int idx1 = len-1;
	int big = sum[len-1];
	FOR(i, len, vals.size()){
		if(sum[i] - sum[i-len] > big){
			big = sum[i] - sum[i-len];
			idx1 = i;
		}
	}
	maxd = big;
	mind = big - len + 1;
	return 1;
}




cv::Mat evaluation_util::target_disparity( Mat & ori, vector<int> & ori_map, vector<int> & tar_map )
{
	Mat tar(ori.size(), CV_32FC1);
	map< int, int> disp_map;
	FOR(i, 0, ori_map.size()){
		disp_map[ori_map[i]] = tar_map[i];
	}
	FOR(y, 0, ori.rows){
		float * ptrOri = ori.ptr<float>(y);
		float * ptrTar = tar.ptr<float>(y);
		FOR(x, 0, ori.cols){
			ptrTar[x] = disp_map[(int)ptrOri[x]];
		}
	}
	return tar;
}

cv::Mat evaluation_util::target_disparity( Mat & ori, map<int, int> curve )
{
	Mat tar(ori.size(), CV_32FC1);
	
	FOR(y, 0, ori.rows){
		float * ptrOri = ori.ptr<float>(y);
		float * ptrTar = tar.ptr<float>(y);
		FOR(x, 0, ori.cols){
			ptrTar[x] = curve[(int)ptrOri[x]];
		}
	}
	return tar;
}

void evaluation_util::MotionHelper::set( int cnt )
{
	m_cnt = cnt;
}

void evaluation_util::MotionHelper::getAngularMotion( Mat & motionXY, Mat & motionZ )
{
	string disp_folder = m_p.m_disparity_folder;
	string motion_folder = m_p.m_motion_folder;
	if(!boost::filesystem::exists(disp_folder+"/"+boost::lexical_cast<string>(m_cnt)+".txt"))
		return ;
	if(!boost::filesystem::exists(motion_folder+"/"+boost::lexical_cast<string>(m_cnt + 1)+".txt"))
		return ;
	Mat m_image_disp;
	Mat m_image_next_disp;

	readMatBinary( m_image_disp, disp_folder+"/"+boost::lexical_cast<string>(m_cnt)+".txt");
	readMatBinary( m_image_next_disp, disp_folder+"/"+boost::lexical_cast<string>(m_cnt + 1)+".txt");

	Mat px, py;
	readMatBinary(px, motion_folder+"/x_"+boost::lexical_cast<string>(m_cnt)+".txt");
	readMatBinary(py, motion_folder+"/y_"+boost::lexical_cast<string>(m_cnt)+".txt");

	ViewParameter vp;
	DisparityConverter dc(vp);

	motionXY.create(px.size(),CV_32FC1);
	motionZ.create(px.size(),CV_32FC1);

	FOR(y,0,motionXY.rows){
		float * ptrX = px.ptr<float>(y);
		float * ptrY = py.ptr<float>(y);
		float * ptrXY = motionXY.ptr<float>(y);
		float * disp = m_image_disp.ptr<float>(y);
		float * ptrZ = motionZ.ptr<float>(y);

		FOR(x,0,motionXY.cols){
			ptrXY[x] = dc.cvtOnDisplayPixel2Angle(Point(x,y), Point(x+ptrX[x], y+ptrY[x]));

			int nx = x + ptrX[x];
			int ny = y + ptrY[x];
			nx = max(0, nx);
			nx = min(motionXY.cols-1, nx);
			ny = max(0, ny);
			ny = min(motionXY.rows-1, ny);
			
			double _initAngle = dc.cvtPixel2Angle(disp[x]);
			double _targetAngle = dc.cvtPixel2Angle(m_image_next_disp.at<float>(ny, nx));
			ptrZ[x] = _targetAngle - _initAngle;

		}
	}
}

void evaluation_util::MotionHelper::getAngularMotion( Mat & d, Mat & nd, Mat & px, Mat & py, Mat & motionXY, Mat & motionZ )
{
	ViewParameter vp;
	DisparityConverter dc(vp);


	motionXY.create(px.size(),CV_32FC1);
	motionZ.create(px.size(),CV_32FC1);

	FOR(y,0,motionXY.rows){
		float * ptrX = px.ptr<float>(y);
		float * ptrY = py.ptr<float>(y);
		float * ptrXY = motionXY.ptr<float>(y);
		float * disp = d.ptr<float>(y);
		float * ptrZ = motionZ.ptr<float>(y);

		FOR(x,0,motionXY.cols){
			ptrXY[x] = dc.cvtOnDisplayPixel2Angle(Point(x,y), Point(x+ptrX[x], y+ptrY[x]));

			int nx = x + ptrX[x];
			int ny = y + ptrY[x];
			nx = max(0, nx);
			nx = min(motionXY.cols-1, nx);
			ny = max(0, ny);
			ny = min(motionXY.rows-1, ny);

			double _initAngle = dc.cvtPixel2Angle(disp[x]);
			double _targetAngle = dc.cvtPixel2Angle(nd.at<float>(ny, nx));
			ptrZ[x] = _targetAngle - _initAngle;

		}
	}
}



evaluation_util::MotionHelper::MotionHelper( _Param mp, int cnt )
{
	m_p = mp;
	m_cnt = cnt;
}

evaluation_util::MotionHelper::MotionHelper()
{

}
