调用接口在new_evaluation.h .cpp中：
double get_cost(_Param mp, int idx, vector< vector<int> > & x_coords, vector< vector<int> > & y_coords, vector<bool> fixed);
参数为： 
_Param mp：  是我用的一个配置文件类，定义在ASCParameter.h, 通过读取config.txt初始化，例如 _Param main_parameter("./config.txt");

注意只需要配置好config.txt中的 WKSPACE 关键值，设为FTP里下载的文件的根目录， mp即可正确提供读取预处理的路径。

int idx： 当前求解的帧下标， 从0开始到frame_cnt-1。

vector< vector<int> > & x_coords 是所有帧的映射曲线的横坐标，每帧横坐标可以设置为范围[-100， 100]。

vector< vector<int> > & y_coords 是所有帧的映射曲线的纵坐标对应值，范围[-100， 100]， 初始化可以定为 x_coords。

vector<bool> fixed 是当前每帧的曲线是否固定，固定为1， 没固定为0。

问题1：如何知道当前处理的视频的总帧数？
文件里保存了一个all_vals.txt,是默认状态下整个视频的不舒适度, 通过以下代码读取，并得到frame_num;

#include "stereoUtil.h"

Mat mat_all_vals;
readMatBinary(mat_all_vals, mp.m_wkspace + "/all_vals.txt");

int frame_num = mat_all_vals.rows;
cout<<frame_num<<endl;



问题2：get_cost调用示例：
#include "ASCParameter.h"
#include "new_evaluation.h"

_Param mp("config.txt");

vector<int> xcoord;
vector<int> ycoord;

FOR(i, -100, 100){
	xcoord.push_back(i);
	ycoord.push_back(1.2*i);
}
vector< vector<int> > xcs;
vector< vector<int> > ycs;
vector<bool> fixed(all_vals.size(), 0);

FOR(i, 0, all_vals.size()){
	xcs.push_back(xcoord);
	ycs.push_back(ycoord);
}

cout<<evaluation::get_cost(mp, 18, xcs, ycs, fixed);
 
