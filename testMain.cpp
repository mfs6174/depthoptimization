#include "Optimizer.h"

extern FrameOptimizer *runningOptimizer;


int main(int argc, char* argv[])
{
  FrameOptimizer worker;
  runningOptimizer=&worker;
  worker.setup("config.txt");
  cout<<worker.videoSize()<<" frames in total"<<endl;
  int id=290;
  if (argc==2)
    id=atoi(argv[1]);
  if (  worker.run(id) )
  {
    for (int i=0;i<worker.getXRange();i++)
      cout<<worker.mapX[id][i]<<"    "<<worker.mapY[id][i]<<"\n";
    cout<<endl;
  }
  return 0;
}
