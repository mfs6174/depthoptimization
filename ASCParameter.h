#ifndef ASCPARAMETER_H
#define ASCPARAMETER_H

#include "ConfigFile.h"
#include "global_header.h"


class _Param{
public:
	_Param();
	_Param(string parameter_file);
	_Param(CConfigFile cfg);

	string m_cmd; 
	string m_wkspace;

	string m_left_video_file;
	string m_right_video_file;
	string m_raw_video_file;
	string m_redcyan_video_file;
	bool m_shangxia;


	
	int m_grid_size;
	string m_matchinfo_file;

	//For triangulation and disparity
	string m_disparity_folder;
	string m_triangulation_folder;
	string m_motion_folder;
	string m_seg_folder;
	string m_raw_motion_mask_file;


	friend ostream& operator << (ostream & f, _Param & mp);

	double w_cfz, w_swv, w_smooth, w_motion, w_nochange;

	int opti_direction;

};



class MainParameter
{
public:
	MainParameter();
	MainParameter(string parameter_file);
	MainParameter(CConfigFile cfg);
	
	string m_cmd; 

	string m_left_video_file;
	string m_right_video_file;
	string m_disparity_folder;
	string m_motioncomfort_folder;
	string m_logfile;
	string m_motion_storage_folder;
	string m_smooth_disp_folder;
	string m_smooth_mc_folder;
	string m_swv_folder;
	string m_comfortzone_folder;
	int m_max_offset_val;

	vector<float> m_offset_vals;

	friend ostream& operator << (ostream & f, MainParameter & mp);

};



class ViewParameter
{
public:
	ViewParameter();
	ViewParameter(string parameter_file);
	ViewParameter(CConfigFile cfg);

	double m_pixelSize;

	double m_pixelNumInWidth;

	double m_pixelNumInHeight;

	double m_viewDistanceInMeter;

	double m_displayHeightInMeter;

	double m_displayWidthInMeter;

	double m_eyeInterVal;

	double m_displayDiagInCun;

//	double m_displayRatioWidth;

//	double m_displayRatioHeight;



	friend ostream& operator << (ostream & f, ViewParameter & vp);

};


class CGParameter
{

public:
	CGParameter();
	CGParameter(string parameter_file);
	CGParameter(CConfigFile cfg);


	double m_maxDisparity;
	double m_minDisparity;
	double m_speedZ;
	double m_speedXY;
	double m_frameRate;
	double m_stimulateTime;

	friend ostream& operator << (ostream & f, CGParameter & cgp);

};






#endif