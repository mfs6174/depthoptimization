#include "ASCParameter.h"
#include <boost/filesystem.hpp>
ViewParameter::ViewParameter():	m_viewDistanceInMeter(0.55), \
							m_eyeInterVal(0.065),\
							m_displayDiagInCun(23.6),\
							m_pixelNumInWidth(1920),\
							m_pixelNumInHeight(1080)
					
{
	//double ScreenWidth = 23.0 * 2.54 / std::sqrt(9.0*9.0+16.0*16.0) * 16.0;
	//double ScreenHeight = 23.0 * 2.54 / std::sqrt(9.0*9.0+16.0*16.0) * 9.0;

	m_displayWidthInMeter = m_displayDiagInCun*2.54 / std::sqrt(m_pixelNumInHeight*m_pixelNumInHeight + m_pixelNumInWidth*m_pixelNumInWidth) * m_pixelNumInWidth / 100;

	m_displayHeightInMeter = m_displayDiagInCun*2.54 / std::sqrt(m_pixelNumInHeight*m_pixelNumInHeight + m_pixelNumInWidth*m_pixelNumInWidth) * m_pixelNumInHeight / 100;

	m_pixelSize = m_displayWidthInMeter / m_pixelNumInWidth;
	
}

ViewParameter::ViewParameter( string parameter_file )
{
	CConfigFile cfg(parameter_file);
	m_pixelNumInWidth = cfg.read<double>("PixelNumWidth");
	m_pixelNumInHeight = cfg.read<double>("PixelNumHeight");
	//m_displayRatioHeight = cfg.read<double>("RatioHeight");
//	m_displayRatioWidth = cfg.read<double>("RatioWidth");
	m_displayDiagInCun = cfg.read<double>("DiagInCun");
	m_eyeInterVal = cfg.read<double>("EyeInterval");

	m_viewDistanceInMeter = cfg.read<double>("ViewDistanceInMeter");

	m_displayWidthInMeter = m_displayDiagInCun*2.54 / std::sqrt(m_pixelNumInHeight*m_pixelNumInHeight + m_pixelNumInWidth*m_pixelNumInWidth) * m_pixelNumInWidth / 100;

	m_displayHeightInMeter = m_displayDiagInCun*2.54 / std::sqrt(m_pixelNumInHeight*m_pixelNumInHeight + m_pixelNumInWidth*m_pixelNumInWidth) * m_pixelNumInHeight / 100;
	
	m_pixelSize = m_displayWidthInMeter / m_pixelNumInWidth;
}

ViewParameter::ViewParameter( CConfigFile cfg )
{
	m_pixelNumInWidth = cfg.read<double>("PixelNumWidth");
	m_pixelNumInHeight = cfg.read<double>("PixelNumHeight");
//	m_displayRatioHeight = cfg.read<double>("RatioHeight");
//	m_displayRatioWidth = cfg.read<double>("RatioWidth");
	m_displayDiagInCun = cfg.read<double>("DiagInCun");
	m_eyeInterVal = cfg.read<double>("EyeInterval");
	m_viewDistanceInMeter = cfg.read<double>("ViewDistanceInMeter");

	m_displayWidthInMeter = m_displayDiagInCun*2.54 / std::sqrt(m_pixelNumInHeight*m_pixelNumInHeight + m_pixelNumInWidth*m_pixelNumInWidth) * m_pixelNumInWidth / 100;

	m_displayHeightInMeter = m_displayDiagInCun*2.54 / std::sqrt(m_pixelNumInHeight*m_pixelNumInHeight + m_pixelNumInWidth*m_pixelNumInWidth) * m_pixelNumInHeight / 100;

	m_pixelSize = m_displayWidthInMeter / m_pixelNumInWidth;
}

ostream & operator<<( ostream & f, ViewParameter & vp )
{
	f<<"#############################"<<endl;
	f<<"#        View Parameter     #"<<endl;
	f<<"#############################"<<endl<<endl;

	f<<"PixelNumWidth: "<<vp.m_pixelNumInWidth<<endl;
	f<<"PixelNumHeight: "<<vp.m_pixelNumInHeight<<endl;

	f<<"DiagInCun: "<<vp.m_displayDiagInCun<<endl;
	f<<"Display Width In Meter:"<<vp.m_displayWidthInMeter<<endl;
	f<<"Display Height In Meter:"<<vp.m_displayHeightInMeter<<endl;

	f<<"Eye Interval: "<<vp.m_eyeInterVal<<endl;
	f<<"View Distance: "<<vp.m_viewDistanceInMeter<<endl;

	return f;

}

CGParameter::CGParameter():m_minDisparity(-2), m_maxDisparity(2), m_speedXY(8), m_speedZ(1), m_stimulateTime(10), m_frameRate(24)
{

}

CGParameter::CGParameter( string parameter_file )
{
	CConfigFile cfg(parameter_file);
	m_maxDisparity = cfg.read<double>("CGMaxDisparity");
	m_minDisparity = cfg.read<double>("CGMinDisparity");
	m_speedXY = cfg.read<double>("CGSpeedXY");
	m_speedZ = cfg.read<double>("CGSpeedZ");
	m_stimulateTime = cfg.read<double>("CGStimulateTime");
	m_frameRate = cfg.read<double>("CGFrameRate");
}

CGParameter::CGParameter( CConfigFile cfg )
{
	m_maxDisparity = cfg.read<double>("CGMaxDisparity");
	m_minDisparity = cfg.read<double>("CGMinDisparity");
	m_speedZ = cfg.read<double>("CGSpeedZ");
	m_speedXY = cfg.read<double>("CGSpeedXY");
	m_stimulateTime = cfg.read<double>("CGStimulateTime");
	m_frameRate = cfg.read<double>("CGFrameRate");
}

ostream & operator<<( ostream & f, CGParameter & cgp )
{
	f<<"#############################"<<endl;
	f<<"#        CG Parameter       #"<<endl;
	f<<"#############################"<<endl<<endl;

	f<<"CGMaxDisparity: "<<cgp.m_maxDisparity<<endl;
	f<<"CGMinDisparity: "<<cgp.m_minDisparity<<endl;
	f<<"CGSpeedXY: "<<cgp.m_speedXY<<endl;
	f<<"CGSpeedZ: "<<cgp.m_speedZ<<endl;
	f<<"CGStimulateTime: "<<cgp.m_stimulateTime<<endl;
	f<<"CGFrameRate: "<<cgp.m_frameRate<<endl;

	return f;

}

MainParameter::MainParameter( string parameter_file )
{
	CConfigFile cfg(parameter_file);
	m_cmd = cfg.read<string>("Command");
	m_left_video_file = cfg.read<string>("LeftVideo");
	m_right_video_file = cfg.read<string>("RightVideo");
	m_disparity_folder = cfg.read<string>("DisparityFolder");
	m_logfile = cfg.read<string>("Log");
	m_motioncomfort_folder = cfg.read<string>("MotionComfortFolder");
	m_motion_storage_folder = cfg.read<string>("MotionStorageFolder");
	m_smooth_disp_folder = cfg.read<string>("SmoothDisparityFolder");
	m_smooth_mc_folder = cfg.read<string>("SmoothMCFolder");

	m_max_offset_val = cfg.read<int>("MaxOffsetVal");

	FOR(i, - m_max_offset_val, m_max_offset_val+1){
		m_offset_vals.push_back(i);
	}
	m_swv_folder = cfg.read<string>("SWVFolder");
	m_comfortzone_folder = cfg.read<string>("ComfortZoneFolder");
}

MainParameter::MainParameter( CConfigFile cfg )
{
	m_cmd = cfg.read<string>("Command");
	m_left_video_file = cfg.read<string>("LeftVideo");
	m_right_video_file = cfg.read<string>("RightVideo");
	m_disparity_folder = cfg.read<string>("DisparityFolder");
	m_logfile = cfg.read<string>("Log");
	m_motioncomfort_folder = cfg.read<string>("MotionComfortFolder");
	m_motion_storage_folder = cfg.read<string>("MotionStorageFolder");
	m_smooth_disp_folder = cfg.read<string>("SmoothDisparityFolder");
	m_smooth_mc_folder = cfg.read<string>("SmoothMCFolder");
	m_max_offset_val = cfg.read<int>("MaxOffsetVal");


	m_max_offset_val = cfg.read<int>("MaxOffsetVal");

	FOR(i, - m_max_offset_val, m_max_offset_val+1){
		m_offset_vals.push_back(i);
	}

	m_swv_folder = cfg.read<string>("SWVFolder");
	m_comfortzone_folder = cfg.read<string>("ComfortZoneFolder");
}

ostream& operator << (ostream & f, MainParameter & mp)
{
	f<<"####################"<<endl;
	f<<"#  Main parameters #"<<endl;
	f<<"####################"<<endl<<endl;

	f<<"Command: "<< mp.m_cmd<<endl;
	f<<"Left Video: "<<mp.m_left_video_file<<endl;
	f<<"Right Video: "<<mp.m_right_video_file<<endl;
	f<<"Disparity Storage Folder: "<<mp.m_disparity_folder<<endl;
	f<<"Motion Storage Folder: "<<mp.m_motion_storage_folder<<endl;
	f<<"Motion Comfort Folder: "<<mp.m_motioncomfort_folder<<endl;
	f<<"Log file: "<<mp.m_logfile<<endl;
	f<<"SmoothDisparity Folder: "<<mp.m_smooth_disp_folder<<endl;
	f<<"SmoothMC Folder: "<<mp.m_smooth_mc_folder<<endl;
	f<<"SWV Folder: "<<mp.m_swv_folder<<endl;
	f<<"ComfortZone Folder: "<<mp.m_comfortzone_folder<<endl;
	f<<"Offset range: ["<< mp.m_offset_vals[0]<<", "<<mp.m_offset_vals.back()<<"]"<<endl;

	return f;
}

_Param::_Param(string parameter_file)
{
	CConfigFile cfg(parameter_file);
	m_cmd = cfg.read<string>("Command");
	m_wkspace = cfg.read<string>("WKSPACE");
	if(!boost::filesystem::exists(m_wkspace))
		boost::filesystem::create_directories(m_wkspace);
	m_left_video_file = cfg.read<string>("LeftVideo");
	m_right_video_file = cfg.read<string>("RightVideo");
	m_raw_video_file = cfg.read<string>("RawVideo");
	m_redcyan_video_file = cfg.read<string>("RedCyanVideo");
	m_shangxia = cfg.read<int>("RawVideoShangxia");
	m_grid_size = cfg.read<int>("GridSize");
	m_matchinfo_file = m_wkspace + "/" + cfg.read<string>("MatchInfo");
	m_disparity_folder = m_wkspace + "/" + cfg.read<string>("DisparityFolder");
	m_motion_folder = m_wkspace + "/" + cfg.read<string>("MotionFolder");
	m_seg_folder = m_wkspace + "/" + cfg.read<string>("SegFolder");	
	m_triangulation_folder = m_wkspace + "/" + cfg.read<string>("TriFolder");
	m_raw_motion_mask_file = cfg.read<string>("RAW_MOTION_MASK_FILE");
	w_cfz = cfg.read<double>("w_cfz");
	w_swv = cfg.read<double>("w_swv");
	w_smooth = cfg.read<double>("w_smooth");
	w_motion = cfg.read<double>("w_motion");
	w_nochange = cfg.read<double>("w_nochange");
	opti_direction = cfg.read<double>("opti_dir");
}

_Param::_Param( CConfigFile cfg )
{
	m_cmd = cfg.read<string>("Command");
	m_wkspace = cfg.read<string>("WKSPACE");
	if(!boost::filesystem::exists(m_wkspace))
		boost::filesystem::create_directories(m_wkspace);
	m_left_video_file = cfg.read<string>("LeftVideo");
	m_right_video_file = cfg.read<string>("RightVideo");
	m_raw_video_file = cfg.read<string>("RawVideo");
	m_redcyan_video_file = cfg.read<string>("RedCyanVideo");
	m_shangxia = cfg.read<int>("RawVideoShangxia");
	m_grid_size = cfg.read<int>("GridSize");
	m_matchinfo_file = m_wkspace + "/" + cfg.read<string>("MatchInfo");
	m_disparity_folder = m_wkspace + "/" + cfg.read<string>("DisparityFolder");
	m_motion_folder = m_wkspace + "/" + cfg.read<string>("MotionFolder");
	m_seg_folder = m_wkspace + "/" + cfg.read<string>("SegFolder");
	m_triangulation_folder = m_wkspace + "/" + cfg.read<string>("TriFolder");
	m_raw_motion_mask_file = cfg.read<string>("RAW_MOTION_MASK_FILE");
	w_cfz = cfg.read<double>("w_cfz");
	w_swv = cfg.read<double>("w_swv");
	w_smooth = cfg.read<double>("w_smooth");
	w_motion = cfg.read<double>("w_motion");
	w_nochange = cfg.read<double>("w_nochange");
	opti_direction = cfg.read<double>("opti_dir");
}

_Param::_Param()
{

}

ostream& operator << (ostream & f, _Param& mp)
{
	f<<"####################"<<endl;
	f<<"#  Main parameters #"<<endl;
	f<<"####################"<<endl<<endl;

	f<<"Command: "<< mp.m_cmd<<endl;
	f<<"WKSpace: "<<mp.m_wkspace<<endl;
	f<<"Left Video: "<<mp.m_left_video_file<<endl;
	f<<"Right Video: "<<mp.m_right_video_file<<endl;
	f<<"Raw Video: "<<mp.m_raw_video_file<<endl;
	f<<"RedCyan Video: "<<mp.m_redcyan_video_file<<endl;
	f<<"Raw Video shangxia: "<<mp.m_shangxia<<endl;
	f<<"Grid Size: "<<mp.m_grid_size<<endl;
	f<<"Match info: "<<mp.m_matchinfo_file<<endl;
	f<<"Trianluation folder: "<<mp.m_triangulation_folder<<endl;
	f<<"Disparity folder: "<<mp.m_disparity_folder<<endl;
	f<<"Motion folder: "<<mp.m_motion_folder<<endl;
	f<<"Seg folder: "<<mp.m_seg_folder<<endl;
	f<<"Raw motion mask file: "<<mp.m_raw_motion_mask_file<<endl;
	f<<"======================"<<endl;
	f<<"w_cfz: "<<mp.w_cfz<<endl;
	f<<"w_swv: "<<mp.w_swv<<endl;
	f<<"w_smooth: "<<mp.w_smooth<<endl;
	f<<"w_motion: "<<mp.w_motion<<endl;
	f<<"w_nochange: "<<mp.w_nochange<<endl;
	return f;
}
