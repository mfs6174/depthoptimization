#ifndef STEREO_UTIL_H
#define STEREO_UTIL_H

#include "ASCParameter.h"


class DisparityConverter
{
public:
	DisparityConverter();

	DisparityConverter(ViewParameter vp);

	void init(ViewParameter vp);

	double cvtPixel2Angle( double pixel );

	double cvtAngle2Pixel( double angle );

	double cvtOnDisplayPixel2Angle(Point2f on_display_init, Point2f on_display_target);

	ViewParameter m_viewParameter;

	double m_zeroAngle;
};

void writeMat(Mat &image, string file, string prefix);
void readMat(Mat &image, string file, string prefix);

bool writeMatBinary(Mat & image, string file);

bool readMatBinary(Mat & image, string file);

int slic(Mat & image, Mat & seg, int region_size = 2500, float compactness = 20);

void stereoFromPairs( Mat & left, Mat & right, Mat & out, int offset);

Mat visualizeBins(vector<float> & vals, int row, int col, int idx = -1);





#endif