#ifndef OPTIMIZER_H
#define OPTIMIZER_H

#include "global_header.h"
#include "StereoUtil.h"
#include "ASCParameter.h"
#include "new_evaluation.h"

#include <ga/ga.h>
#include <ga/GARealGenome.h>

float Gobjective(GAGenome &_gg);

class FrameOptimizer
{
 public:
  //dataArea
  vector<vector<int> > mapX,mapY;
  vector<bool> fixed;

  //configArea
  bool fixSeed;
  int mapStartScale;
  int mapSegScale;
  //int mapStartBits;
  int mapSegNum;
  //int mapSegBits;
  int xScaleNeg,xScalePos;
  
  //GA Area
  int popsize;
  int ngen;
  float pmut;
  float pcross;
  int scoreF,flushF;
  //GASimpleGA ga;
  //GASteadyStateGA ga;
  GASigmaTruncationScaling scaling;


  //interface
  FrameOptimizer();
  void setup(const string &configName,int preRunLen=1);
  bool run(const vector<int> &_runList,const vector<int> &_startList=vector<int>(),const vector<int> &_endList=vector<int>());
  bool run(int runID,int depthStart=0,int depthEnd=0);
  int videoSize() {return frameNum;}
  int getXRange() {return xNum;}
  
  //objective
  float objective(GAGenome &_gg);
 private:
  int xNum;
  bool ready;
  int runLen;
  Mat mat_all_vals;
  int frameNum;
  _Param mp;
  vector<int> runList;
  vector<int> startList,endList;
  
  //util
  void _translate(const vector<int> &from, vector<int> &to,int xBegin,int xEnd);
  //void _initGA();
  int _clip(int x);

};

#endif
