#include "MotionComfort.h"
#include <math.h>

MotionComfort::MotionComfort():m_speedXY(8), m_speedZ(1), m_disparity(0), m_luminanceFrequency(4)
{

}

MotionComfort::MotionComfort( double vxy, double vz, double d, double lf )
{
	init(vxy, vz, d, lf);
}



void MotionComfort::init( double vxy, double vz, double d, double lf )
{
	m_speedXY = fabs(vxy);
	m_speedZ = fabs(vz);
	m_disparity = d;
	m_luminanceFrequency = fabs(lf);
	truncateParameters();
}

double MotionComfort::motionComfort( bool include_frequency )
{
	double speed_term = 0;
	double luminance_term = 0;

	speed_term = -0.0556 * m_speedXY + (-0.6042) * m_speedZ + 0.0191 * m_speedXY * m_speedZ + 4.6567;
	if(m_disparity>0){
		speed_term += (0.0022 * m_disparity * m_speedXY + 0.1833 * m_disparity * m_speedZ + (-0.6932)* m_disparity); 
	}else{
		speed_term += ( (-0.0043) * m_disparity * m_speedXY + (-0.1001) * m_disparity * m_speedZ + 0.2303 * m_disparity);
	}

	double log_lf = log(m_luminanceFrequency)/log(10.f);
							
	
	luminance_term = (0.9925 * log_lf * log_lf - 1.1599 * log_lf);
	if(include_frequency){
		return speed_term + luminance_term;
	}else return speed_term;
}

void MotionComfort::truncateParameters()
{
	if(m_speedXY>16)
		m_speedXY = 16;
	if(m_speedZ>2)
		m_speedZ = 2;
	if(m_disparity<-2)
		m_disparity = -2;
	else if(m_disparity>2)
		m_disparity = 2;
	if(m_luminanceFrequency>16)
		m_luminanceFrequency = 16;
	else if ( m_luminanceFrequency<1)
		m_luminanceFrequency = 1;
}

double MotionComfort::cost()
{
	return motionComfort(1);
}


