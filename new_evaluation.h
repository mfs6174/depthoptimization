#ifndef NEW_EVALUATION_H
#define NEW_EVALUATION_H

#include <opencv2/opencv.hpp>


#include "StereoUtil.h"
#include "MotionComfort.h"
#include "ExcessiveDepthCost.h"

using namespace std;
using namespace cv;

namespace evaluation{

double stereo_motion_term(Mat & disparity_map, Mat & motion2DX, Mat & motion2DY, vector<int> & ori_disp, vector<int> & target_disp);




/*  Comfort zone evaluation term */
/*
input:	Disparity Map [CV_32FC1]
		Original Disparity values
		Target Disparity values
output: comfort zone penalty value		
*/
double comfort_zone_term(Mat & disparity_map, vector<int> & ori_disp, vector<int> & target_disp);



/*Stereoscopic Window Violation Term*/
/*
input	Disparity Map [CV_32FC1]
		Reverse Disparity Map [CV_32FC1]
		Original Disparity values
		Target Disparity values
output   stereoscopic window violation value
*/

double stereo_window_violation_term(Mat & disparity_map, Mat & reverse_disparity_map, vector<int> & ori_disp, vector<int> & target_disp);

double stereo_window_violation_term2(Mat & disparity_map, Mat & reverse_disparity_map, vector<int> & ori_disp, vector<int> & target_disp);

double neighbour_smooth_term(Mat & disparity_map1, vector<int> & ori_disp1, vector<int> & target_disp1,
	Mat & disparity_map2, vector<int> & ori_disp2, vector<int> & target_disp2);

double neighbour_curve_similarity_term(vector<int> & ori_disp1, vector<int> & target_disp1, vector<int> & ori_disp2, vector<int> & target_disp2);

double do_not_change_term(Mat & disparity1, vector<int> & ori_disp1, vector<int> & tar_disp1);

/*
double motion_comfort_term_mask(Mat & mask, Mat & pX, Mat & pY, Mat & pD1, vector<int> & map_ori1, vector<int> & map_tar1, Mat & pD2, vector<int> & map_ori2, vector<int> & map_tar2);

double motion_comfort_term_saliency(Mat & saliency, Mat & pX, Mat & pY, Mat & pD1, vector<int> & map_ori1, vector<int> & map_tar1, Mat & pD2, vector<int> & map_ori2, vector<int> & map_tar2);*/

double motion_comfort_term(Mat & seg, Mat & pX, Mat & pY, Mat & pD1, vector<int> & map_ori1, vector<int> & map_tar1, Mat & pD2, vector<int> & map_ori2, vector<int> & map_tar2);

/*
void optimize( _Param mp, \
	int start_frame, vector<int> start_xcoord, vector<int> start_ycoord, \
	int end_frame, vector<int> end_xcoord, vector<int> end_ycoord, \
	vector<int> to_opti_frames, vector< vector<int> > to_opti_xcoords,  vector< vector<int> >  to_opti_ycoords );*/


/*
double get_cost( _Param mp, \
	int start_frame, vector<int> start_xcoord, vector<int> start_ycoord, \
	int end_frame, vector<int> end_xcoord, vector<int> end_ycoord, \
	vector<int> to_opti_frames, vector< vector<int> > to_opti_xcoords,  vector< vector<int> >  to_opti_ycoords);*/

double get_cost(_Param mp, int idx, vector< vector<int> > & x_coords, vector< vector<int> > & y_coords, vector<bool> fixed);

double get_cost(_Param mp, int idx, vector< vector<int> > & x_coords, vector< vector<int> > & y_coords);

bool get_depth_range(_Param mp, int idx, float & mind, float & maxd);


}

namespace evaluation_util{
	
	Mat target_disparity(Mat & ori, vector<int> & ori_map, vector<int> & tar_map);
	Mat target_disparity(Mat & ori, map<int, int> curve);

	class MotionHelper
	{
	public:
		MotionHelper();
		MotionHelper(_Param mp, int cnt);
		void set(int cnt);
		void getAngularMotion(Mat & motionXY, Mat & motionZ);
		void getAngularMotion(Mat & d, Mat & nd,  Mat & px, Mat & py, Mat & motionXY, Mat & motionZ);
		
		_Param m_p;
		int m_cnt;
	};
}



#endif
