#include "Optimizer.h"
#include <ga/GARealGenome.C>

FrameOptimizer *runningOptimizer;

inline void printVec(const vector<int> &a)
{
  for (int i=0;i<a.size();i++)
    cout<<a[i]<<' ';
  cout<<endl;
}

float Gobjective(GAGenome &_gg)
{
  return runningOptimizer->objective(_gg);
}


float FrameOptimizer::objective(GAGenome &_gg)
{
  GARealGenome & gg = (GARealGenome &)_gg;
  double y=0;
  for (int i=0;i<runList.size();i++)
  {
    vector<int> pinput(mapSegNum+1,0);
    for (int j=0;j<=mapSegNum;j++)
    {
      pinput[j]=cvRound(gg.gene(i*(1+mapSegNum)+j ));
    }
    _translate(pinput,mapY[runList[i]],startList[i],endList[i]);
    //printVec(pinput);
    //printVec(mapX[runList[i]]);
    //printVec(mapY[runList[i]]);
  }
  for (int i=0;i<runList.size();i++)
    y+=evaluation::get_cost(mp, runList[i], mapX, mapY);
  cout<<"eval "<<y<<endl;
  return y;
}



bool FrameOptimizer::run(int runID,int _depthStart,int _depthEnd)
{
  float depthStart,depthEnd;
  if (_depthEnd!=_depthStart)
  {
    depthStart=_depthStart;
    depthEnd=_depthEnd;
  }
  else
  {
    evaluation::get_depth_range(mp,runID,depthStart,depthEnd);
    //cout<<depthStart<<' '<<depthEnd<<endl;
  }
  vector<int> tmp(1,runID),tmp1(1,int(depthStart)),tmp2(1,int(depthEnd));
  return run(tmp,tmp1,tmp2);
}

inline int FrameOptimizer::_clip(int x)
{
  if (x<xScaleNeg)
    return xScaleNeg;
  if (x>xScalePos)
    return xScalePos;
  return x;
}

bool FrameOptimizer::run(const vector<int> &_runList,const vector<int> &_startList,const vector<int> &_endList)
{
  if (!ready)
  {
    cout<<"not initialized"<<endl;
    return ready;
  }
  runList=vector<int>(_runList);
  if (! (_startList.empty() || _endList.empty() ) )
  {
    startList=vector<int>(_startList);
    endList=vector<int>(_endList);
  }
  else
  {
    startList.clear();
    endList.clear();
    for (int i=0;i<runList.size();i++)
    {
      float mind,maxd;
      evaluation::get_depth_range(mp,runList[i],mind,maxd);
      startList.push_back(int(mind));
      endList.push_back(int(maxd));
    }
  }
    
  double y=0;
  for (int i=0;i<runList.size();i++)
  {
    y+=evaluation::get_cost(mp, runList[i], mapX, mapY);
    startList[i]=_clip(startList[i]);
    endList[i]=_clip(endList[i]);
  }
  cout<< "score before optimize linear "<<y<<endl;
  y=0;
  for (int i=0;i<runList.size();i++)
  {
    for (int j=0;j<mapY[runList[i]].size();j++)
      mapY[runList[i]][j]=0;
    y+=evaluation::get_cost(mp, runList[i], mapX, mapY);
  }
  cout<< "score before optimize 0 "<<y<<endl;
  y=0;
  for (int i=0;i<runList.size();i++)
  {
    for (int j=0;j<mapY[runList[i]].size();j++)
      mapY[runList[i]][j]=mapX[runList[i]][j]*0.7;
    y+=evaluation::get_cost(mp, runList[i], mapX, mapY);
  }
  cout<< "score before optimize 0.5 "<<y<<endl;
  runLen=runList.size();

  //_initGA();
  GARealAlleleSetArray asa1;
  for (int i=0;i<runLen;i++)
  {
    mapSegScale=cvRound((endList[i]-startList[i])*2.0/mapSegNum);
    //cout<<endList[i]<<' '<<startList[i]<<endl;
    asa1.add(startList[i]-mapSegScale,startList[i]+mapSegScale,1);
    for (int j=0;j<mapSegNum;j++)
      asa1.add(0,mapSegScale,1);
  }
  GARealGenome genome1(asa1,Gobjective);
  GASteadyStateGA ga(genome1);
  ga.minimize();
  ga.populationSize(popsize);
  ga.nGenerations(ngen);
  ga.pMutation(pmut);
  ga.pCrossover(pcross);
  //ga.scaling(scaling);
  ga.scoreFilename("frame.dat");
  ga.scoreFrequency(scoreF);
  ga.flushFrequency(flushF);
  cout<<ga.pReplacement()<<endl;

  
  if (fixSeed)
    ga.evolve(7);
  else
    ga.evolve();

  genome1 = ga.statistics().bestIndividual();
  cout << "the ga found an optimum\n";
  cout<< "best score "<<genome1.score()<<' '<<Gobjective(genome1)<<endl;
  return true;
 }


void FrameOptimizer::_translate(const vector<int> &from, vector<int> &to,int xBegin,int xEnd)
{
  to=vector<int>(xNum,0);
  int sp;
  for (int i=0;i<xNum;i++)
  {
    if (i+xScaleNeg>xBegin)
      break;
    to[i]=from[0];
    sp=i;
  }
  int seglen=(xEnd-xBegin)/mapSegNum;
  int seglast=xEnd-xBegin-seglen*(mapSegNum-1);
  for (int i=0;i<mapSegNum-1;i++)
  {
    int c=0;
    for (int j=i*seglen+1;j<=(i+1)*seglen;j++)
    {
      c++;
      to[sp+j]=cvRound(to[i*seglen+sp]+double(from[i+1])/seglen*c);
    }
  }
  int c=0;
  for (int j=(mapSegNum-1)*seglen+1;sp+j<to.size();j++)
  {
    if (sp+j+xScaleNeg>xEnd)
    {
      to[sp+j]=to[sp+j-1];
      continue;
    }
    c++;
    to[sp+j]=cvRound(to[(mapSegNum-1)*seglen+sp]+double(from[mapSegNum])/seglast*c);
  }
}


void FrameOptimizer::setup(const string &configName,int preRunLen)
{
  mp=_Param(configName);
  runLen=preRunLen;
  readMatBinary(mat_all_vals, mp.m_wkspace + "/all_vals.txt");
  frameNum = mat_all_vals.rows;
  if (frameNum<1)
  {
    cout<<"we do not read the frame values succussfully"<<endl;
    return;
  }
  vector<int> tmp;
  xNum=xScalePos-xScaleNeg+1;
  for (int i=xScaleNeg;i<=xScalePos;i++)
    tmp.push_back(i);
  mapX=vector< vector<int> >(frameNum,vector<int>(tmp));
  mapY=vector< vector<int> >(frameNum,vector<int>(tmp));
  fixed=vector<bool> (frameNum,false);
  //_initGA();

  ready=true;
}

FrameOptimizer::FrameOptimizer()
{
  fixSeed=true;
  mapStartScale=100;
  //mapStartBits=8;
  mapSegNum=8;
  mapSegScale=30;
  //mapSegBits=9;
  xScaleNeg=-100;
  xScalePos=100;

  popsize  = 30;
  ngen     = 10;
  pmut   = 0.015;
  pcross = 0.8;
  scoreF=1;
  flushF=1;

  frameNum=0;
  ready=false;
}
  
