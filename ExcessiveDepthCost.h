#ifndef EXCESSIVE_DEPTH_H
#define EXCESSIVE_DEPTH_H

#include <cmath>
#include <cstdio>
#include <iostream>
#include "ASCParameter.h"

class ExcessiveDepthCost
{
public:
	ExcessiveDepthCost(double disparity, double viewdistance = 0.55, double eyeinterval = 0.065);
	
	
	bool isExcessive();
	double cost();

	double m_disparity;
	double m_thresFar;
	double m_thresNear;
	double m_viewDistance;
	double m_eyeInterval;
	
	static double m_near;
	static double m_far;
	static double T_near;
	static double T_far;
};



#endif