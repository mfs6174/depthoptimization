#include "ExcessiveDepthCost.h"

double ExcessiveDepthCost::m_near = 1.035;

double ExcessiveDepthCost::m_far = 1.129;

double ExcessiveDepthCost::T_near = -0.626;

double ExcessiveDepthCost::T_far = 0.442;


ExcessiveDepthCost::ExcessiveDepthCost( double disparity, double viewdistance /*= 0.5*/, double eye_interval /*= 0.065*/ )
{
	m_disparity = disparity;
	m_viewDistance = viewdistance;
	m_eyeInterval = eye_interval;
}

bool ExcessiveDepthCost::isExcessive()
{
	m_thresFar = 2 * atan(0.5 * m_eyeInterval / m_viewDistance)  - 2 * atan( 0.5* m_eyeInterval * (1 - T_far * m_viewDistance )/(m_far*m_viewDistance)); 
	m_thresNear = 2 * atan(0.5 * m_eyeInterval / m_viewDistance)  - 2 * atan( 0.5* m_eyeInterval * (1 - T_near * m_viewDistance )/(m_near*m_viewDistance)); 
	m_thresFar *= (180/3.1415926);
	m_thresNear*= (180/3.1415926);
	//std::cout<<m_thresNear<<" "<<m_thresFar<<std::endl;
	if( m_disparity< m_thresFar && m_disparity > m_thresNear)
		return 0;
	else return 1;
}

double ExcessiveDepthCost::cost()
{
	return (double)isExcessive();
}
