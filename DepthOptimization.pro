TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
QMAKE_CXXFLAGS += -std=c++11

SOURCES += \
    ga/GA1DArrayGenome.C \
    ga/GA1DBinStrGenome.C \
    ga/GA2DArrayGenome.C \
    ga/GA2DBinStrGenome.C \
    ga/GA3DArrayGenome.C \
    ga/GA3DBinStrGenome.C \
    ga/GAAllele.C \
    ga/GABaseGA.C \
    ga/GABin2DecGenome.C \
    ga/gabincvt.C \
    ga/GABinStr.C \
    ga/GADCrowdingGA.C \
    ga/GADemeGA.C \
    ga/gaerror.C \
    ga/GAGenome.C \
    ga/GAIncGA.C \
    ga/GAList.C \
    ga/GAListBASE.C \
    ga/GAListGenome.C \
    ga/GAParameter.C \
    ga/GAPopulation.C \
    ga/garandom.C \
    ga/GARealGenome.C \
    ga/GAScaling.C \
    ga/GASelector.C \
    ga/GASimpleGA.C \
    ga/GASStateGA.C \
    ga/GAStatistics.C \
    ga/GAStringGenome.C \
    ga/GATree.C \
    ga/GATreeBASE.C \
    ga/GATreeGenome.C \
    ASCParameter.cpp \
    ConfigFile.cpp \
    ExcessiveDepthCost.cpp \
    MotionComfort.cpp \
    new_evaluation.cpp \
    Optimizer.cpp \
    StereoUtil.cpp \
    testMain.cpp \
    SLIC.cpp

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    ga/ga.h \
    ga/GA1DArrayGenome.h \
    ga/GA1DBinStrGenome.h \
    ga/GA2DArrayGenome.h \
    ga/GA2DBinStrGenome.h \
    ga/GA3DArrayGenome.h \
    ga/GA3DBinStrGenome.h \
    ga/GAAllele.h \
    ga/GAArray.h \
    ga/GABaseGA.h \
    ga/GABin2DecGenome.h \
    ga/gabincvt.h \
    ga/GABinStr.h \
    ga/gaconfig.h \
    ga/GADCrowdingGA.h \
    ga/GADemeGA.h \
    ga/gaerror.h \
    ga/GAEvalData.h \
    ga/GAGenome.h \
    ga/gaid.h \
    ga/GAIncGA.h \
    ga/GAList.h \
    ga/GAListBASE.h \
    ga/GAListGenome.h \
    ga/GAMask.h \
    ga/GANode.h \
    ga/GAParameter.h \
    ga/GAPopulation.h \
    ga/garandom.h \
    ga/GARealGenome.h \
    ga/GAScaling.h \
    ga/GASelector.h \
    ga/GASimpleGA.h \
    ga/GASStateGA.h \
    ga/GAStatistics.h \
    ga/GAStringGenome.h \
    ga/GATree.h \
    ga/GATreeBASE.h \
    ga/GATreeGenome.h \
    ga/gatypes.h \
    ga/gaversion.h \
    ga/std_stream.h \
    ASCParameter.h \
    ConfigFile.h \
    ExcessiveDepthCost.h \
    global_header.h \
    MotionComfort.h \
    new_evaluation.h \
    Optimizer.h \
    StereoUtil.h \
    SLIC.h


unix: CONFIG += link_pkgconfig
unix: PKGCONFIG += opencv

LIBS += \
       -lboost_system\
       -lboost_filesystem\
       -lopencv_nonfree\
